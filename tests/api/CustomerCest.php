<?php

use \Codeception\Util\HttpCode;

class CustomerCest
{
    public function _before(ApiTester $I)
    {

    }

    // tests
    public function createCustomer(\ApiTester $I)
    {
        $data = [
            'name' => 'Lera',
            'firstname' => 'Valeria',
            'lastname' => 'Zinovieva',
            'phone' => '+79782223344',
            'email' => 'lera@gmail.com'
        ];

        $I->haveHttpHeader('Content-Type', 'application/json');
        $I->sendPost('/api/customer', $data);
        $I->seeResponseCodeIs(HttpCode::CREATED); // 201
        $I->seeResponseIsJson();
        $I->seeResponseContains(json_encode("customerId"));

    }

    public function getCustomer(\ApiTester $I)
    {
        $data = [
            'name' => 'Lera',
            'firstname' => 'Valeria',
            'lastname' => 'Zinovieva',
            'phone' => '+79782223344',
            'email' => 'lera@gmail.com'
        ];

        $I->sendGet('/api/customer/3c7282c6-21f7-11ec-bcae-d8d090037524');
        $I->seeResponseCodeIs(HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains(json_encode($data));

    }

    public function noGetCustomer(\ApiTester $I) //негативный тест
    {
        $data = [
            'name' => 'Lera',
            'firstname' => 'Valeria',
            'lastname' => 'Zinovieva',
            'phone' => '+79782223344',
            'email' => 'lera@gmail.com'
        ];

        $I->sendGet('/api/customer/36');
        $I->seeResponseCodeIs(HttpCode::NOT_FOUND); // 404
        $I->seeResponseIsJson();
        $I->seeResponseContains(json_encode('Customer not found'));

    }

    public function deleteCustomer(\ApiTester $I)
    {
        $I->sendDelete('/api/customer/3c7282c6-21f7-11ec-bcae-d8d090037524');
        $I->seeResponseCodeIs(HttpCode::OK); // 200
        $I->seeResponseIsJson();
        $I->seeResponseContains(json_encode('Customer deleted successfully'));
    }

    public function noDeleteCustomer(\ApiTester $I)
    {
        $I->sendDelete('/api/customer/9');
        $I->seeResponseCodeIs(HttpCode::NOT_FOUND); // 404
        $I->seeResponseIsJson();
        $I->seeResponseContains(json_encode('Customer not found'));

    }
}
